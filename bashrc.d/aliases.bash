# bash aliases

# ls shortcuts
alias ls='/bin/ls -G'
alias ll='ls -l'
alias la='ls -A'
alias lla='ls -lA'
alias ll.='ls -ld .[^$.]*'
alias l.='ls -d .[^$.]*'
alias l1='ls -1'
alias lm='ls -m'

# one-liners
alias c='clear'
alias h='history'
alias j='jobs'
alias x='exit'

# general
alias sbash='[ -f ~/.bash_profile ] && . ~/.bash_profile'

# command modifications

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias tree='/usr/local/bin/tree -C'


#

alias cask='brew cask'
alias wifi='/System/Library/PrivateFrameworks/Apple80211.framework/Versions/A/Resources/airport -s'
alias wificon='/System/Library/PrivateFrameworks/Apple80211.framework/Versions/A/Resources/airport -I '

alias playground='[ -f ~/Developer/xcode/playground.playground ] && open ~/Developer/xcode/playground.playground'
alias icloud='cd "/Users/kend/Library/Mobile Documents"; clear;pwd;lm'

alias xctest='swift test 2>&1 | grep -E "[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9.]+" | grep -v "started at "'
alias xcbuild='swift build'
alias geek='open -a "GeekTool"'
#alias cron='crontab -e'
#alias cronl='crontab -l'

alias iobase='ssh iobase'
alias safari='open -a "Safari"'
alias www='open -a "Safari"'
alias ical='calcurse'



alias bashenv='cd "${bashenv:?}"; clear; pwd; ll; git status'
alias dev='cd "${dev:?}"; clear; pwd; ll'
alias docs='cd "${docs:?}";clear;pwd;ls'
alias desk='cd "${desk:?}";clear;pwd;ls'
