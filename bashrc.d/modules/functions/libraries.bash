#!/usr/local/bin/bash

function libraries
{
	
	#sudo find {,${HOME},/System}/Library -cmin -30 -type f \! -empty -iname "*log*" -not -path "*/*.*/*"

	local -i OPTIND STARTSECONDS ENDSECONDS DURATIONSECONDS
	local -i cmin mmin
	local filePattern excludePattern
	OPTIND=1

	while getopts :c:m:n:x: opt; do
		case ${opt} in
			c) [[ -z ${mmin:-} ]] && cmin=${OPTARG};;
			m) [[ -z ${cmin:-} ]] && mmin=${OPTARG};;
			n) filePattern="${OPTARG}";;
			x) excludePattern="${OPTARG}";;
			:) echo "ERROR: flag requires argument -- ${OPTARG}" >&2 && return 1;;
			?) echo "ERROR: invalid flag -- ${OPTARG}" >&2 && return 1;;
		esac
	done
	shift $((OPTIND-1))

	local cmd

	cmd="sudo /usr/bin/find {,/System,${HOME}}/Library -mindepth 1"

	if [[ -n ${cmin:-} ]]; then
		cmd+=" -cmin -${cmin}"
	elif [[ -n ${mmin:-} ]]; then
		cmd+=" -mmin  ${mmin}"
	fi

	if [[ -n "${filePattern:-}" ]]; then
		cmd+=" -name \"${filePattern}\""
	fi

	if [[ -n "${excludePattern:-}" ]]; then
		cmd+=" -not -name \"${excludePattern}\""
	fi

	echo "EUID: ${EUID}"
	echo "COMMAND:"; echo -e "\\n\\t${cmd}\\n"

	STARTSECONDS=${SECONDS} \
		&& eval "${cmd}" \
		&& ENDSECONDS=${SECONDS} \
		&& DURATIONSECONDS=$((ENDSECONDS-STARTSECONDS))

	echo -e "\\n\\tSearch duration (sec.): ${DURATIONSECONDS}\\n"

	return $?
}
