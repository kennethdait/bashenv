#!/usr/bin/env bash

# filetimes

function filetimes
{
    
    usage()
    {
        local msg
        msg="\\n\\t[31;1musage:[0m\\n"
        msg+="\\t\$ filetimes -[options] [targetDir]"
        echo -e "${msg}\\n"
        return $?
    }
    
#    if [[ ! -d "$targetDir" || ! -x "$targetDir" ]]; then
#        echo "ERROR..." >&2
#        return 1
#    fi

    caseOpts()
    {
        case "${opt}" in
            a)
                listOneTimeType "Access"
                ;;
            m)
                listOneTimeType "Modify"
                ;;
            c)
                listOneTimeType "Change"
                ;;
            u)
                usage
                return $?
                ;;
            ?)
                echo "ERROR: invalid flag -- ${OPTARG}" >&2
                return 1
                ;;
        esac
        return $?
    }

    listOneTimeType()
    {
        local targetDir="${PWD}"
        local fileTimeType="${1:?}"
        local headerTitle
        case ${fileTimeType} in
            "Access") headerTitle="Last Access Times:";;
            "Modify") headerTitle="Last Modified Times:";;
            "Change") headerTitle="Last Changed Times:";;
            *) return 1;;
        esac

        echo -e "\\n\\t----[31;1m${headerTitle}[0m----\\n"

        while read -r line
        do
            printf "%q\n" "$line"
        done \
              \
               < <(
                    /bin/ls -1GTt "${targetDir}" | sed "s:^:$PWD/:"
                   ) \
                      | xargs -L1 stat -x \
                      | grep -e 'File:' -e "${fileTimeType}:" \
                      | sed -e "s:^${fileTimeType}\:[[:blank:]]*::" -e 's:^[[:blank:]]*File\:[[:blank:]]*::' -e 's:^\(".*\)$:|\1; :' \
                      | tr -d '\n' \
                          | tr '|' '\n' \
                          | tr ';' '|' \
                      | sed 's:[[:blank:]]*\(|\)[[:blank:]]*:\1:g' \
                      | awk -F'|' '{
                          
                              printf ("\t%-24s\t%s\n",$2,$1)
                        }' \
                            \
                             | tr -d '"'
        echo
    }

    listAllFileTimes()
    {
        local targetDir="${1:-$PWD}"
        echo TARGETDIR: ${targetDir}
        for fileTimeType in Access Modify Change
        do
            
            local headerTitle
            case ${fileTimeType} in
                "Access") headerTitle="Last Access Times:";;
                "Modify") headerTitle="Last Modified Times:";;
                "Change") headerTitle="Last Changed Times:";;
            esac

            echo -e "\\n\\t----[31;1m${headerTitle}[0m----\\n"
            while read -r line
            do
                printf "%q\n" "$line"
            done \
                  \
                   < <(
                        /bin/ls -1GTt "${targetDir}" | sed "s:^:$PWD/:"
                       ) \
                          | xargs -L1 stat -x \
                          | grep -e 'File:' -e "${fileTimeType}:" \
                          | sed -e "s:^${fileTimeType}\:[[:blank:]]*::" -e 's:^[[:blank:]]*File\:[[:blank:]]*::' -e 's:^\(".*\)$:|\1; :' \
                          | tr -d '\n' \
                              | tr '|' '\n' \
                              | tr ';' '|' \
                          | sed 's:[[:blank:]]*\(|\)[[:blank:]]*:\1:g' \
                          | awk -F'|' '{
                              
                                  printf ("\t%-24s\t%s\n",$2,$1)
                            }' \
                                \
                                 | tr -d '"'
            echo
        done
    }

    if [ $# -eq 0 ]; then listAllFileTimes "$PWD"
    elif [[ "${1:0:1}" =~ ^-$ ]]; then
        local -i OPTIND
        OPTIND=1
        while getopts :uamc opt; do
            caseOpts
        done
    else
        local targetDir="${1:?}"
        listAllFileTimes "$targetDir"
    fi
    
    return $?

}
