#!/bin/bash

# interfaces


function interfaces {

	usage() {
		local msg
		msg="this is the usage statement"
		echo -e "\\n\\t$msg\\n\n"
		return $?
	}

	printHeader() {
		echo -e "\\n\\t[31;1m System Interfaces: [0m\\n"
	}
	
	printOneLines() {
		printHeader
		if [[ $# -gt 0 && "$1" == '-t' ]]; then
			# print truncated
			ifconfig | sed '/^[^[:blank:]]/s:^:+:' \
				| tr '\n' '|' \
				| tr '+' '\n' \
				| sed 's:[[:blank:]]*|[[:blank:]]*: | :g' \
				| while read -r line; do
					printf "%-*.*s\n" $(tput cols) $(tput cols) "$line"
				  done \
				| sort -t':' -k1
		else
			# non-truncated
			ifconfig \
				| sed '/^[^[:blank:]]/s:^:+:' \
				| tr '\n' '|' \
				| tr '+' '\n' \
				| sed 's:[[:blank:]]*|[[:blank:]]*: | :g'
		fi
		return $?
	}

	if [ $# -eq 0 ]; then
		# default behavior, when no parameters are passed:
		#	- print truncated one-line output
		printOneLines -t
		return $?
	else

		caseOpt() {
			case ${opt} in
				u|h) usage; return $?;;
				t) printOneLines -t; return $?;;
				o) printOneLines; return $?;;
				f) ifconfig;;
				l) ifconfig | ${PAGER:-/usr/bin/less};;
				?) echo "ERROR: invalid flag -- ${OPTARG}" >&2; return 1;;
			esac
			return
		}

		local -i OPTIND
		OPTIND=1
		while getopts :utofhl opt; do
			caseOpt
		done
		shift $((OPTIND-1))
		return
	fi
}


