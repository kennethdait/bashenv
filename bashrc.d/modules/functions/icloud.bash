#!/usr/bin/env bash
# vim: ts=4 sw=4 sts=4 et
#
# icloud.bash
# USER BASH FUNCTION MODULE
#
# Created by Kenneth Dait on 10/10/2018
#

function icloud {

  local -r icloudRootDir='/Users/kend/Library/Mobile Documents'
  local -r icloudSubDirectoryRoot='/Users/kend/Library/Mobile Documents'
  local -i OPTIND; OPTIND=1

  usage() {
    echo "this is the usage"
    return $?
  }

  listICloudDriveContentsRaw() {

    listTopLevelFiles() {
      /bin/ls -1 "$icloudRootDir" | sed "s:^:${icloudRootDir}/:"
      return $?
    }

    listTopLevelDirectories() {
      /bin/ls -1 "$icloudSubDirectoryRoot" | sed "s:^:${icloudSubDirectoryRoot}/:"
      return $?
    }

    listFullPaths() {
      listTopLevelFiles; listTopLevelDirectories
      return $?
    }

    listDirsThenFiles() {
      getContainingDirs() {
        local data result; local -i resultLines; data="${1:?}"
        result="$(echo -e "${data}" \
          | while read -r line; do echo -e "${line%/*}"; done \
          | sort | uniq)"
        resultLines=$(echo -e "${result}" | wc -l | cut -f1 \
          | sed -e 's:^[[:blank:]]*::' -e 's:[[:blank:]]*$::')
        if [[ ${resultLines} -eq 1 ]]; then
          echo "$result"; return 0
        else
          return 1
        fi
      }
      printOutputSections() {
        local title data
        title="${1:?}"
        data="${2:?}"
        echo -e "${data}" | sed "s:${title}/::" | sed -e "1i\\
        \\
        \\	[31;1m ${title} [0m\\
        \\
        " -e '$a\
        \
        '
      }

      local fileList fileListContainingDir
      local dirList dirListContainingDir ;
      dirList="$(listTopLevelDirectories)"
      dirListContainingDir="$(getContainingDirs "$dirList")" || return 1
      fileList="$(listTopLevelFiles)"
      fileListContainingDir="$(getContainingDirs "$fileList")" || return 1

      printOutputSections "$dirListContainingDir" "$dirList"
      printOutputSections "$fileListContainingDir" "$fileList"

      return $?
    }

    if [ $# -eq 0 ]; then
      listFullPaths; return $?
    else
      case "$1" in
        '-f')
          listDirsThenFiles; return $?
          ;;
        *)
          listFullPaths; return $?
          ;;
      esac
    fi
    return $?
  }

  listICloudDriveContentsFormatted() {
    listICloudDriveContentsRaw -f
    # listICloudDriveContentsRaw -f | sed 's:^:	   - :' | sed -e '1i\
    # \
    # \	[31;1m iCloud Contents: [0m\
    # \
    # ' -e '$a\
    # \
    # '
  }

  handleOpts() {
    while getopts :uhglfr opt; do
      case ${opt} in
        l|f) listICloudDriveContentsFormatted || return 1;;
        r) listICloudDriveContentsRaw || return 1;;
        u|h) usage; return $?;;
        g) cd "${icloudDir}" || return 1;;
        ?) echo "ERROR: flag was not recognized -- ${OPTARG}"; return 1;;
      esac
    done; return $?
  }

  if [[ "${1:0:1}" =~ ^-$ ]]; then
    #
    # getopts
    #
    local -i OPTIND;  OPTIND=1
    echo "handle opts"
    handleOpts "$@"
    shift $((OPTIND-1))
  elif [ $# -eq 0 ]; then
    #
    # default action: no parameters passed
    # - show the usage statement
    #
    usage; return $?
  fi
}
