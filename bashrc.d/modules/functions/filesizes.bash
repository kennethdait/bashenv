#!/usr/local/bin/bash

filesizes ()
{
    declare report
    if [[ $# -eq 0 ]]; then
        targetDirs=("$PWD");
    else
        targetDirs=("$@");
    fi;

    report="\\n\\tFILESIZES (MB)\\n\\n"
    for dir in "${targetDirs[@]}";
    do
        report+="$(du -s "$dir" | awk '{SIZE=($1*512);printf ("\t%-30.30s\t%2.2f MB\n",$NF,(SIZE/1000000))}')\\n"
    done

    report+="\\n"
    echo -e "$report"
}
