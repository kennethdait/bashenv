#!/usr/local/bin/bash
#
# BASH FUNCTION: newalias
#

newalias ()
{
	main ()
	{
        local aliasCmd
        aliasCmd="alias ${1:?}='${2:?}'"
        echo -en "Is this correct:\\n\\n\\t${aliasCmd}"
        read -p "? [y/N] " response
        if [[ "${response}" =~ ^[yY] ]]; then
            if
                eval "$(echo "${aliasCmd}")"
            then
                echo -e "\\n\\tSUCCESS\\n$(alias "${1}")\\n"
                return 0
            else
                echo "ERROR: unable to create alias (${aliasCmd})" >&2
                return 1
            fi
        else
            echo "Aborting..." >&2; return 1
        fi
		return 5
	}

	usage ()
	{
		local msg
		msg="\nUSAGE: newalias\n\t"
		msg+="$ newalias\n\n"

		echo -e "${msg}"
		return $?
	}

	handleOpts ()
	{
		while getopts :he opt; do
			case ${opt} in
				h) usage; return 0;;
                e)
                    #edit alias file
                    local aliasFile
                    aliasFile=${bashenv:?}/bashrc.d/aliases.bash
                    if [[ -f "${aliasFile}" ]]; then
                        ${EDITOR:-vim} "${aliasFile}"
                        return 0
                    else
                        echo "ERROR: unable to find alias file (${aliasFile})" >&2
                        return 1
                    fi
                    ;;
				*) echo "ERROR: invalid flag -- ${OPTARG}" >&2; return 1;;
			esac
		done
		return $?
	}

	local -i OPTIND
	OPTIND=1

	handleOpts "$@"
	shift $((OPTIND-1))

	main "$@"

	return $?
}
