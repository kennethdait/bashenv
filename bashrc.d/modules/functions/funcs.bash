#!/usr/local/bin/bash
# funcs

function funcs {

	local -r functionDir=${BASHENV}/bashrc.d/modules/functions

	if ! [ -d "$functionDir" ]; then
		echo "ERROR: FUNCTION DIRECTORY NOT FOUND -- ${functionDir}" >&2
		return 1
	fi

	usage() {
		local msg
		msg='\n\tfuncs USAGE:\n\n'
		msg+='\t  $ funcs -[options]\n\n'
		echo -e "$msg"
		return $?
	}

	defaultBehavior() {
		[ -x "$functionDir" ] && cd "$functionDir" || return 1
		return 0
	}

	editFunctionFile() {
		local target="${1:?}"
		target+="*"
		local queried
		queried="$(find "$functionDir" -mindepth 1 -type f -name "$target")"
		if [[ $(echo "$queried" | wc -l) -eq 1 ]]; then
			echo "Editing function file... ${queried}"
			${EDITOR:-/usr/bin/vim} "${queried}"
			return 0
		else
			echo "ERROR: there was an error finding file to edit\\n\\nFound results:\\n${queried}" >&2
			return 1
		fi
	}

	listRawPathNames() {
		find "$functionDir" -mindepth 1 -maxdepth 1 -type f \! \( -name ".*" -or -name "_*" \) \! -empty
		return
	}

	listNameParts() {
		while read -r filePath; do
			local fileName
			fileName="${filePath##*/}"
			base="${fileName%.*}"
			printf "\t%-10s\t%s\t%s\n" "$base" "$fileName" "$filePath"
		done < <(listRawPathNames)
		return
	}

	listFunctionFiles() {
		echo -e "\\n\tFUNCTIONS:\\n\\t(${functionDir})\\n" | sed "s:${HOME}:~:"
		listNameParts | awk '{NAME=$1; $1=""; $2=""; printf("\t%-10s\t%s\n",NAME,$3)}' | sed "s:$functionDir/::"
		echo -en '\n\n'
		return $?
	}

	deleteFile ()
	{
		_confirmDeletion ()
		{
			echo "Deleting file: ${targetFile}?"
			read -p "Enter to proceed... [cntrl-c to cancel] " response
			if [[ -z "$response" ]]; then
				#
				# proceed to delete
				#
				declare tmpFile
				tmpFile="/tmp/$(basename "${targetFile}")"
				if [[ -f "${tmpFile}" ]]; then tmpFile="${tmpFile}.$(date +%s)"; fi
				if [[ -f "${targetFile}" && ! -f "${tmpFile}" ]]; then
					cp "${targetFile}" /tmp/"$(basename "${targetFile}")"
					rm "${targetFile}"
				elif [[ -f "${targetFile}" ]]; then
					rm "${targetFile}"
				fi
				if [[ ! -f "${targetFile}" ]]; then
					echo "Deleted file: ${targetFile}"
				else
					echo "ERROR: an error occurred trying to delete file -- ${targetFile}" >&2
				fi
			fi
		}

		local targetFile
		targetFile="${1:?}"

		echo "file to delete: ${targetFile}"

		local result
		result="$(find "${functionDir}" -name "${targetFile}*")"
		if [[ $(echo -e "$result" | wc -l | cut -f1 | tr -dc '0-9') -eq 1 ]]; then
			targetFile="$result"
		else
			echo "ERROR: unable to find file to delete (${result})" >&2
			return 1
		fi

		if [[ -f "${targetFile}" ]]; then
			_confirmDeletion; return $?
		else
			echo "ERROR" >&2; return 1
		fi
		return 5
	}

	selectFileToModify() {

		editFile ()
		{
			echo "Editing file: ${1:?}..."
			${EDITOR:-/usr/bin/vim} "${1:?}"
		}



		local -u targetAction
		targetAction="${1:?}"
		echo -e "\\n\\tSELECT FUNCTION FILE TO ${targetAction}:\\n"
		select file in $(listNameParts | awk '{print $1}') ABORT; do
			echo
			if [[ "$file" == 'ABORT' ]]; then
				echo "Aborting..." >&2; return 1
			else

				local results
				results=""

				if [[ -z "${file}" && -n "${REPLY}" ]]; then file="$REPLY"; fi
				results="$(listRawPathNames | grep "${file}" | grep -v -e '^$')"

				if [[ $(echo "$results" | wc -l) -eq 1 ]]; then
					#
					# only one file found
					#

					if [[ "${targetAction}" == "EDIT" ]]; then
						editFile "${results}"
					elif [[ "${targetAction}" == "DELETE" ]]; then
						deleteFile "${results}"
					fi
					return $?
				else
					echo -e "ERROR: unable to select file to edit\\nsearch results:\\n${results}" >&2
					return 1
				fi
				return 3
			fi
			break
		done
		return 4
	}

	populateNewFunctionFile ()
	{
		local targetPath functionName
		targetPath="${1:?}" functionName="${2:?}"

		[[ ! -s "${targetPath}" ]] || return 1
		cat <<EOF >> "$targetPath"
#!/usr/local/bin/bash
#
# BASH FUNCTION: ${functionName}
#

${functionName} ()
{
	main ()
	{
		# begin code...
		return 0
	}

	usage ()
	{
		local msg
		msg="\\nUSAGE: ${functionName}\\n\\t"
		msg+="\$ ${functionName}\\n\\n"

		echo -e "\${msg}"
		return \$?
	}

	handleOpts ()
	{
		while getopts :h opt; do
			case \${opt} in
				h) usage; return 0;;
				*) echo "ERROR: invalid flag -- \${OPTARG}" >&2; return 1;;
			esac
		done
		return \$?
	}

	local -i OPTIND
	OPTIND=1

	handleOpts "\$@"
	shift \$((OPTIND-1))

	main "\$@"

	return \$?
}
EOF
		return $?
	}

	newFunctionFile()
	{
		local targetName targetFilename
		targetName="${1:?}"

		# append "*.bash" extension, if not already present
		#
		if [[ ! "$targetName" =~ \.bash$ ]]; then
			targetFilename="${targetName}.bash"
		else
			targetFilename="${targetName}"
		fi

		local targetPath="${functionDir}/${targetFilename}"
		if [[ ! -f "${targetPath}" ]]; then
			touch "$targetPath"
		else
			#
			# ERROR: File exists
			#
			echo "ERROR: target exists -- ${targetPath}" >&2
			return 1
		fi
		if
			populateNewFunctionFile "${targetPath}" "${targetName%%\.bash}"
		then
			echo "Successfully created function file: ${targetPath}"
			return 0
		else
			echo "ERROR: an error occurred creating new file (${targetPath})" >&2
			return 1
		fi
		return 3
	}

	handleOpts() {
		while getopts :he:n:ld: opt; do
			case ${opt} in
				d)
					deleteFile "${OPTARG}"
					return $?
					;;
				h)
					usage
					return $?
					;;
				e)
					editFunctionFile "${OPTARG}"
					return $?
					;;
				l)
					listFunctionFiles
					return $?
					;;
				n)
					newFunctionFile "${OPTARG}"
					;;
				:)
					case ${OPTARG} in
						e)
							selectFileToModify "edit"
							return $?
							;;
						d)
							selectFileToModify "delete"
							return $?
							;;
						n)
							read -p "Enter function name: [abort] " response
							if [[ -z "${response:-}" ]]; then
								echo "Aborting..." >&2; return 1
							else
								newFunctionFile "${response}"
							fi
							return 0
							;;
						*)
							echo "ERROR: flag requires an argument -- ${OPTARG}" >&2
							return 1
							;;
						esac
					;;
				*)
					echo "ERROR: invalid flag -- ${OPTARG}" >&2
					return 1
					;;
			esac
		done
	}

	if [ $# -eq 0 ]; then
		defaultBehavior
		return $?
	else
		local -i OPTIND
		OPTIND=1
		handleOpts "$@"
		shift $((OPTIND-1))
	fi

	return $?

}
