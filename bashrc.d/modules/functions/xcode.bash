#!/usr/bin/env bash

# xcode

function xcode
{

    openFile()
    {
        local target="${1:?}"
        echo "Opening xcode project: ${target}..."
        open -a "Xcode" "${target}"
        return $?
    }

    if [ $# -eq 0 ]
    then
        open -a "Xcode"
    else
        local -i OPTIND
        OPTIND=1
        while getopts :f: opt
        do
            case ${opt} in
                f)
                    openFile "${OPTARG}"
                    ;;
                :)
                    if [[ "${OPTARG}" == 'f' ]]; then
                        if [[ $(find "$PWD" -maxdepth 1 -mindepth 1 -name "*.xcodeproj" | wc -l) -eq 1 ]]
                        then
                            openFile *.xcodeproj
                            return 0
                        else
                            echo "ERROR: found multiple projects in current working directory" >&2
                            return 1
                        fi
                    fi
                    echo "ERROR: flag requires an argument -- ${OPTARG}" >&2
                    return 1
                    ;;
                ?)
                    echo "ERROR: invalid flag -- ${OPTARG}" >&2
                    return 1
                    ;;
            esac
        done
    fi
}
