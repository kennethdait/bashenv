#!/usr/local/bin/bash
#
# BASH FUNCTION: trash
#

trash ()
{

#	main ()
#	{
#		return 0
#	}

	usage ()
	{
		local msg
		msg='\nUSAGE: trash\n\n\t'
		msg+='$ trash -[OPTIONS]\n\n'
        msg+='\tOPTIONS:\n'
        msg+='\t  -h\tdisplay this help message\n'
        msg+='\t  -i\tdisplay trash info (size, etc)\n'
        msg+='\t  -e\tempty the trash\n'
        msg+='\n'
		echo -e "${msg}"
		return $?
	}

    printTrashInfo()
    {
        echo -e '\n\t\033[31;1mTRASH INFO\033[0m (~/.Trash):'
        local sizeHR=$(du -sh ~/.Trash | cut -f1)
        local sizeB=$(du -s ~/.Trash | cut -f1)
        echo -e "\n\tSIZE: ${sizeHR} (${sizeB} bytes)\n"

        local fileTypeCounts="$(getTrashDirectoryFileTypeCounts)"
        echo -e "${fileTypeCounts}" | sed 's:^:	- :'

        echo -e "\n	- CREATION DATE RANGE: $(getCreationDateRanges)"


        echo
        return 0
    }

    getCreationDateRanges()
    {
        ll -tTU $dl \
            | sed -e '1d' -e "s:^.*${USER}[[:blank:]][[:blank:]]*[[:alpha:]]*[[:blank:]]*[[:digit:]]*[[:blank:]]*::" \
            | sed -E -e 's:(^.*[0-9]+\:[0-9]+\:[0-9]+[[:blank:]]*[0-9]{4}).*$:\1:' \
            | tr '\t' ' ' \
            | tr -s ' ' \
            | cut -d" " -f1-2 -f4 \
            | awk -F" " '{print $3$1$2}' \
            | sed -E -e '/[[:alpha:]][0-9]{1}$/s:([[:alpha:]])([0-9]{1})$:\10\2:' \
            | sed -E \
                -e 's:Jan:01:' \
                -e 's:Feb:02:' \
                -e 's:Mar:03:' \
                -e 's:Apr:04:' \
                -e 's:May:05:' \
                -e 's:Jun:06:' \
                -e 's:Jul:07:' \
                -e 's:Aug:08:' \
                -e 's:Sep:09:' \
                -e 's:Oct:10:' \
                -e 's:Nov:11:' \
                -e 's:Dec:12:' \
            | sort -n \
            | sed -n -e '1p' -e '$p' \
            | tr "\n" "-" \
            | sed -e 's:-: & :' -e 's:-$::' \
            | sed -E -e 's:([0-9]{4})([0-9]{2})([0-9]{2}):\1.\2.\3:g';
        return
    }


    getTrashDirectoryFileTypeCounts()
    {
        for i in 'd' 'f' 'l'; do
            declare -i typeCount=$(find ~/.Trash -mindepth 1 -type ${i} | wc -l | cut -f1 | sed 's:^[[:blank:]]*::')
            declare str=""
            case $i in
                d) str+="DIRECTORIES: ";;
                f) str+="FILES: ";;
                l) str+="LINKS: ";;
            esac
            str+=${typeCount}
            echo "${str}"
        done
        return
    }


    emptyTrash()
    {
        getTrashSize()
        {
            echo "$(du -sh ~/.Trash | cut -f1 | sed 's:^[[:blank:]]*\([^[:blank:]]\)[[:blank:]]*$:\1:')"
            return
        }

        getFileCount()
        {
            echo $(find ~/.Trash -mindepth 1 | wc -l | cut -f1 | sed 's:^[[:blank:]]*\([0-9]*\)[[:blank:]]*:\1:')
            return
        }

        deleteTrashContents()
        {
            cd ~/.Trash
            rm -rf ./*
            if [[ $(ls -lA | wc -l | tr -dc '0-9') -eq 0 ]]; then echo "Trash has been successfully emptied"
            else echo "An error occurred while emptying trash directory. Abort." >&2
            fi
            cd ~-
            return
        }

        promptToDelete()
        {
            echo -en "\\n\\tAre you sure you want to clear trash?
            \\t - SIZE: ${trashSize}
            \\t - TOTAL FILES: ${fileCountToDelete}
            \\033[31;1mPROCEED?\\033[0m"
            read -p " (y/N) " RESPONSE
            echo -en "\\n\\t"
            return
        }

        parseResponse()
        {
            case ${RESPONSE} in
                y|Y|yes|YES)
                    deleteTrashContents
                    ;;
                *)
                    echo -e "Aborting..." >&2
                    ;;
            esac
            echo
            return
        }

        local fileCountToDelete
        local trashSize
        local RESPONSE
        trashSize="$(getTrashSize)"
        fileCountToDelete=$(getFileCount)
        promptToDelete
        parseResponse
        return
    }

	handleOpts ()
	{
		while getopts :hie opt; do
			case ${opt} in
				h) usage; return 0;;
                i) printTrashInfo;;
                e) emptyTrash;;
				*) echo "ERROR: invalid flag -- ${OPTARG}" >&2; return 1;;
			esac
		done
		return $?
	}

	local -i OPTIND
	OPTIND=1

	handleOpts "$@"
	shift $((OPTIND-1))

	//main "$@"

	return $?
}
