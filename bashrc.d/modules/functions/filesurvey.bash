#!/usr/local/bin/bash

filesurvey () 
{ 

    usage()
    {
        local usageMsg
        usageMsg+="\\n\\tUSAGE: filesurvey -[OPTIONS] [targetDir]?\\n"

        echo -e "$usageMsg"
        return 0
    }

    handleOpts()
    {
        while getopts :ch opt; do case ${opt} in
                c) outputFilter="count";;
                h) usage; return 0;;
                *) echo "ERROR: invalid flag -- ${OPTARG}" >&2;;
        esac done
        return 0
    }

    buildReport()
    {
        reportOutput="$(find "$targetDir" -type f -exec file -b "{}" \; | awk -F, '{print $1}' | sort | uniq -c | sort -n)"
        totalCount=$(echo $(($(echo "$reportOutput" | sed 's:^[[:blank:]]*\([0-9][0-9]*\).*$:\1:' | tr '\n' '+' | sed 's:+$::'))))

        if [[ "${outputFilter:-NULL}" =~ ^count$ ]]; then
            reportOutput="\\n\\t${totalCount} total files (in target directory: ${targetDir})\\n"; return 0
        else
            reportOutput="$(echo -e "$reportOutput" | sed 's:^:	:')\\n"
            reportOutput="\\n\\tFILE SURVEY: ${targetDir}\\n\\n${reportOutput}\\n\\t(TOTAL: ${totalCount})\\n";
            return 0
        fi
        return 1
    }

    #--------- 0. declare vars
    declare outputFilter targetDir reportOutput; declare -i totalCount OPTIND
    OPTIND=1

    #--------- 1. handle args
    handleOpts "$@"; shift $((OPTIND-1))


    #--------- 2. verify target directory
    targetDir="${1:-$PWD}"
    if [[ ! -d "$targetDir" || ! -x "$targetDir" ]]; then echo "ERROR: unable to find or read target directory -- ${targetDir}" >&1; return 1; fi


    #--------- 3. build and output report
    buildReport
    echo -e "${reportOutput}"

    return 0
}

