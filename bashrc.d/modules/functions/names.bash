#!/bin/bash

function names {
	local lhn hn cn
	lhs="$(scutil --get LocalHostName 2>&1)"
	hn="$(scutil --get HostName 2>&1)"
	cn="$(scutil --get ComputerName 2>&1)"
	getNameType() {
		case "${1:?}" in
			0) echo LocalHostName;;
			1) echo HostName;;
			2) echo ComputerName;;
		esac
		return 0
	}
	local -i c
	c=0
	for name in "$lhs" "$hn" "$cn"; do
		local result=""
		if ! echo "$name" | grep -q 'not set'; then
			# is set
			result="$name"
		else
			result='--nil--'
		fi
		printf "%15s  =>   %s\n" "$(getNameType ${c})" "$result"; unset result
		((c++))
	done
	return $?
}
