#!/usr/local/bin/bash
#
# BASH FUNCTION: caches
#

caches ()
{
	main ()
	{
        echo -e "\\n\\t(requires admin privileges)\\n"
		sudo du -sm {/Library,${HOME}/Library,/System/Library}/Caches/* | sort -n \
            | sed 's:^[0-9]*:& MB:'
        return 0
	}

	usage ()
	{
		local msg
		msg="\nUSAGE: caches\n\t"
		msg+="$ caches\n\n"

		echo -e "${msg}"
		return $?
	}

	handleOpts ()
	{
		while getopts :h opt; do
			case ${opt} in
				h) usage; return 0;;
				*) echo "ERROR: invalid flag -- ${OPTARG}" >&2; return 1;;
			esac
		done
		return $?
	}

	local -i OPTIND
	OPTIND=1

	handleOpts "$@"
	shift $((OPTIND-1))

	main "$@"

	return $?
}
