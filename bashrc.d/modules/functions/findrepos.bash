#!/usr/local/bin/bash

function findrepos
{
	declare -i startSec endSec durSec
	startSec=$SECONDS
	echo -e "\\n\\t[31;1m STARTED (${startSec}) [0m\\n\\n" \
		&& sudo find {{$HOME,/usr},{,$HOME,/System}/Library} -type d -name ".git" -exec dirname {} \; \
		&& endSec=${SECONDS} \
		&& durSec=$((endSec-startSec)) \
		&& echo -e "\\n\\t[31;1m ENDED (${endSec}) [0m\\n" \
		^^ echo "\\n\\t[31;1mCOMMAND DURATION: ${durSec} seconds\\n\\n"
	return
}
