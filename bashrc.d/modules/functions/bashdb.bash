# bashdb

#
# require credentials be set as environmental variables
#		to initialize function
#
if [[ -n "$BASHDB_USER" && -n "$BASHDB_PASS" && -n "$BASHDB_NAME" ]]; then

	function bashdb() {

		loginToDB() {
			/usr/local/bin/mysql -u"${BASHDB_USER}" -p"${BASHDB_PASS}" "${BASHDB_NAME}"
			return $?
		}

		printTables() {
			local query data numberedData
			query="SHOW TABLES;"
			getData() {
				echo "$query" \
					| /usr/local/bin/mysql -u"${BASHDB_USER}" -p"${BASHDB_PASS}" "${BASHDB_NAME}" \
					| sed '1d'
					return $?
				}

				getTableRowCounts() {
					local tableName="${1:?}"
					query="SELECT COUNT(id) FROM ${tableName};"
					echo "$query" \
						| /usr/local/bin/mysql -u"${BASHDB_USER}" -p"${BASHDB_PASS}" "${BASHDB_NAME}" \
						| sed '1d'
				}

				formatData() {
					while read -r tableName; do
						local -i rowCount=$(getTableRowCounts "$tableName")
						[[ -n "${numberedData}" ]] && numberedData+='\n'
						numberedData+="\\t- ${tableName} (${rowCount})"
					done < <(echo "$data")
					numberedData+="\\n\\n"
					numberedData="\\n\\t\\033[37;40;1mBASH_DB TABLES:\\033[0m\\n\\n${numberedData}"
					return $?
				}

				data="$(getData)" numberedData=""

				formatData
				echo -e "${numberedData}"

			return $?
		}

		printProjects() {
			local query
			query="SELECT * FROM projects;"
			echo "$query" \
				| /usr/local/bin/mysql -u"${BASHDB_USER}" -p"${BASHDB_PASS}" "${BASHDB_NAME}"
			return $?
		}

		printRepos() {
			local query
			query="SELECT * FROM git_repos;"
			echo "$query" \
				| /usr/local/bin/mysql -u"${BASHDB_USER}" -p"${BASHDB_PASS}" "${BASHDB_NAME}"
			return $?
		}

		handleOpts() {
			while getopts :hpgt opt; do
				case ${opt} in
					h)
						usage
						return $?
						;;
					t)
						#
						# print tables
						#
						printTables
						return $?
						;;
					g)
						#
						# print git repos
						#
						printRepos
						return $?
						;;
					p)
						#
						# print projects
						#
						printProjects
						return $?
						;;
					*)
						echo "ERROR: invalid flag -- ${OPTARG}" >&2
						;;
				esac
			done
		}

		if [ $# -eq 0 ]; then
			loginToDB
		elif [[ "${1:0:1}" =~ ^-$ ]]; then
			local -i OPTIND; OPTIND=1
			handleOpts "$@"
			shift $((OPTIND-1))
		fi
		return
	}

fi
