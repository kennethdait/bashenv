#!/bin/bash
#
# vbash
#

function vbash {

	usage() {
		local msg
		msg="\\n\\tUSAGE: vbash\\n\\n\\t\$ vbash -[options] [commands]\\n\\n"
		echo -e "$msg"
		return $?
	}

	listComponents() {
		find $BASHENV -type f \! \( -path "*/.git/*" \) \
			| sed "s:^$BASHENV/::" \
			| awk -F"/" '{printf("%-20s\t(.../%s)\n",$NF,$0)}' \
			| sort | sed -e 's:^:	- :' \
			| while read -r line; do
					count=$(echo "$line" \
						| awk '{print $NF}' \
						| wc -c | cut -f1)
						[[ $(echo "$line" | grep -v "^[[:blank:]]*$" | wc -l) -gt 0 ]] \
							&& line="${count}\\t${line}"
						echo -e "$line"
				done \
			| sort -n | sed 's:^[[:blank:]]*[0-9][0-9]*::' | sed -e '1i\
			\
			\	[40;36;1m BASHENV Component Files: [0m \
			\
			' -e '$a\
			\
			'
		return $?
	}

	handleOpts() {
		while getopts :hul opt; do
			case ${opt} in
				h|u)
					usage; return $?
					;;
				l)
					listComponents
					return $?
					;;
				?)
					echo "ERROR: invalid flag -- ${OPTARG}" >&2
					return 1
			esac
		done
	}

	handleCommands() {
		local filter; filter="${1:?}"
		local queryResults target; local -i queryCount
		queryResults="$(find "${BASHENV}" -type f \! \( -path "*/.git/*" -or -name ".*" -or -name "_*" \) -name "${filter}*")"
		queryCount="$(echo -e "${queryResults}" | wc -l | cut -f1 | sed -e 's:^[[:blank:]]*::' -e 's:[[:blank:]]*$::')"
		if [[ ${queryCount} -eq 1 ]]; then
			#
			# result found
			#
			target="${queryResults}"
			if [[ -r "${queryResults}" && -w "${queryResults}" ]]; then
				echo "BASHENV: editing component -- ${target}..."
				${EDITOR:-/usr/bin/vim} "${queryResults}"
			fi
		else
			echo "ERROR: unable to resolve target component file..." >&2
			echo "${queryResults}" >&2
			return 1
		fi
	}

	if [ $# -eq 0 ]; then
		usage; return $?
	elif [[ "${1:0:1}" =~ ^-$ ]]; then
		local -i OPTIND; OPTIND=1
		handleOpts "$@" || return 1
	fi

	[ $# -gt 0 ] && handleCommands "$@"

	return $?
}
