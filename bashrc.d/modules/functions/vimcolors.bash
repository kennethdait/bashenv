#vim colors


function vimcolors {
	l1 /usr/share/vim/vim80/colors | sed 's:\.vim$::' | grep -v '\.txt$' | xargs -L4 printf "%-10s\t%-10s\t%-10s\t%-10s\n" | sed -e '1i\
	\
	\	[31;1mVIM COLORS[0m\
	\
	' -e '$a\
	\
	' | sed 's:^[^[:blank:]]:	&:'
	return $?
}
