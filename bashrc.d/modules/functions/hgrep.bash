#!/usr/local/bin/bash

# hgrep

function hgrep
{

    usage()
    {
        local msg
        msg = "USAGE:\n \$ hgrep [search term]"
        msg += "\n\t- works like \`pgrep'"
        echo -e "$msg"
        return 0
    }

    if [[ -z "${1-}" ]]; then
        usage; return 1
    fi

    local query results cmd; local -i resultCount
    query="${1:?}"

    results="$(history | /usr/bin/grep --color=always "$query")"
    resultCount=$(echo "${resultCount}" | wc -l | cut -f1 | tr -d "$IFS")
    
    echo -e "\\n\\t[31;1m RESULTS [0m\\n${results}\\n\\n"
    echo -e "\\n\\t${resultCount}\\n"

    return  $?

}
