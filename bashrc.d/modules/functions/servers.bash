#!/usr/local/bin/bash

servers ()
{
    getWifiStatus () {
        /System/Library/PrivateFrameworks/Apple80211.framework/Versions/A/Resources/airport -I \
            | grep -E B?SSID | sort | awk -F": " '{print $NF}' | tr '\n' '\t' | awk -Ft '{printf ("%s (%s)\n",$1,$2)}'
        return $?
    }

    collectStatuses ()
    {
        wifiStatus="$(getWifiStatus)"
        if [[ -n "$wifiStatus" ]]; then
            servers+=( "wifi: [32;1monline[0m (${wifiStatus})" )
        else
            servers+=( "wifi: [31;1moffline[0m" )
        fi

        for url in moonbase.dev kenanigans.com
        do
            if [[ -n "${wifiStatus}" && $(curl -sSI http://${url} | sed -n '1p' | cut -d" " -f2) -eq 200 ]]; then
                servers+=( "${url}: [32;1monline[0m" )
            else
                servers+=( "${url}: [31;1moffline[0m" )
            fi
        done

        if [[ -n "$(ssh iobase 'uptime' 2> /dev/null)" ]]; then
            servers+=( "iobase: [32;1monline[0m" )
        else
            servers+=( "iobase: [31;1moffline[0m" )
        fi
        return $?
    }

    reportServerStatus ()
    {
        echo -e "\\nSERVER STATUS:"
        for s in "${servers[@]}"; do echo -e "- ${s}"; done | \
            sed 's:moonbase\.dev:& (localhost):'
        echo
        echo -e "(updated: $(date -r $timestamp +%Y-%m-%d\ %H:%M))\\n"
    }

    declare wifiStatus
    declare -a servers

    declare timestamp
    collectStatuses && timestamp=$(date +%s)

    reportServerStatus

    return $?
}
