#!/usr/local/bin/bash

# md

if [ -d /Applications/iA\ Writer.app ]; then
    function md
    {

        local target="${1:?}"
        if [ -f "$target" ];
        then
            open -a "iA Writer" "$target"
        fi
        return  $?

    }
fi
