#!/usr/local/bin/bash


# playground()

function playground
{

	echo blah
	openPlayground()
	{
		echo unzipping...
		tar -xzvf "$playgroundArchive" -C /tmp
		local -r playgroundInstance=/tmp/MyPlayground.playground
		[[ ! -d "$playgroundInstance" ]] || echo "ERROR - instance file note found: ${playgroundInstance}" >&2 \
			&& return 1
		/bin/ls -lAGF "$playgroundInstance"
		trap '[[ -d "$playgroundInstance" ]] && rm -rf "$playgroundInstance"' RETURN INT QUIT
		echo opening...
		open "$playgroundInstance"
		return
	}

	local -r playgroundArchive=~/Developer/data/xcode-playground.tar.gz
	[[ ! -f "$playgroundArchive" ]] || echo "ERROR - template file note found: ${playgroundArchive}" >&2 \
		&& return 1
	

	#
	# unarchive playground to tmp folder
	#

	#openPlayground &> /dev/null &
	openPlayground

	return $?
}
