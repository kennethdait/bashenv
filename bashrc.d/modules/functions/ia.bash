#!/bin/bash
# vim: ts=4 sw=4 sts=4 et ai nu
# ia

if [[ -d /Applications/iA\ Writer.app ]]; then
	function ia {
        
        local iCloudProxy=~/Library/Favorites/icloud
        usage() {
            echo "this is the usage"
            return $?
        }


        local -i OPTIND; OPTIND=1
        while getopts :oihu opt; do
            case ${opt} in
                h|u) usage; return $?;;
                o) open -a /Applications/iA\ Writer.app;;
                i) goToICloutLibrary; return $?;;
                ?) echo "ERROR: invalid flag -- ${OPTARG}" >&2; return 1;;
            esac
        done
        shift $((OPTIND-1))
	}
fi
