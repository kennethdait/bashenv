#!/usr/local/bin/bash

function net
{
	truncated()
	{
		ifconfig | sed '/^[^[:blank:]]/s:$:+:' \
			| nl -ba | sed '/+$/s:^[[:blank:]]*\([0-9][0-9]*\)[[:blank:]]*:\1\::' \
			| awk -F: '/^[^0-9]/{print} /^[0-9]/{print $2":"$1":"$3}' \
			| sed '/+$/s/:[[:blank:]]*/:/g' \
			| sed '/+$/s/[[:blank:]][[:blank:]]*mtu[[:blank:]]*\([0-9][0-9]*\)/mtu\1/' \
			| sed -e '/+$/s:<:\::' -e 's:>\([^>]*$\):\:\1:' \
			| sed "$(
			
			ifconfig \
				| sed '/^[^[:blank:]]/s:$:+:' | nl -ba \
				| sed '/+$/s:^[[:blank:]]*\([0-9][0-9]*\)[[:blank:]]*:\1\::' \
				| awk -F: '/^[^0-9]/{print} /^[0-9]/{print $2":"$1":"$3}' \
				| sed '/+$/s/:[[:blank:]]*/:/g' \
				| sed '/+$/s/[[:blank:]][[:blank:]]*mtu[[:blank:]]*\([0-9][0-9]*\)/mtu\1/' \
				| sed -e '/+$/s:<:\::' -e '/+$/s:>\([^>]*$\):\:\1:' \
				| grep -e ":$" -e 'member:' -e '[0-9] path cost [0-9]' \
				| sort -n | cut -f 1 \
				| sed 's:^[ ]*::' | sort -n \
				| sed -n -e '1p' -e '$p' | tr '\n' ',' \
				| sed 's:,$::'
			
			)d" | sed '/+$/s:^:|:' | sed '/+$/s:\+$:\:&:' \
			| sed '/[^+]$/s:$:++:' | sed 's:+$::' | sed '/+$/s:^[[:blank:]]*\([0-9][0-9]*\)[[:blank:]]*:\1\::' \
			| tr '\n' ':' | tr '|' '\n' | sed 's:\(^[^:]*\)\::	\1	:' | sed -e 's:\([^[:alpha:]]\)\(active\)\([^[:alpha:]]\):\1 \2\3:' \
			| sed 's:[[:blank:]]*\(status\:\)[[:blank:]]*\(inactive\): |[31;1m\1 \2[0m|:' \
			| sed '/[^n]active/s:[[:blank:]]*\(status\:\)[[:blank:]]*\(active\): |[32;1m\1 \2[0m|:g' \
			| sed 's/:inet6.*:nd6 //' | sed 's:[0-9][0-9]*\:[^|]*|\([^|][^|]*\)|:\1:' | sed 's:[[:punct:]]*$::' \
			| sed 's:<[^>]*$::' | awk '
						/[^n]active/{print "1"$0}
						/inactive/{print "2"$0}
						!/active/ && NF!=0{print "3"$0}' \
			| sort -n \
			| sed 's:^[0-9][0-9]*::' | sed -e '1i\
				\
				\	[40;37;1m NETWORK STATUS [0m\
				\
				' -e '$a\
				\
				'
		return $?
	{

	printFullIFConfig()
	{
		ifconfig
		return $?
	}

	if [[ "${1:-}" == '-f' ]]
	then
		printFullIFConfig
	else
		truncated
	fi

	return $?

}

