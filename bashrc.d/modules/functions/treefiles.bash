#!/usr/local/bin/bash


function treefiles
{
	local -i OPTIND ALLFILES ISCOLORED
	local targetDir
	OPTIND=1 ALLFILES=1 ISCOLORED=0

	if [[ $# -gt 0 && "${1:0:1}" =~ ^-$ ]]; then
		while getopts :an opt; do
			case ${opt} in
				a)
					ALLFILES=0
					;;
				n)
					ISCOLORED=1
					;;
				?)
					echo "ERROR: invalid flag -- ${OPTARG}" >&2
					return 2
					;;
			esac
		done
		shift $((OPTIND-1))
	fi
	if [[ $# -gt 0 ]]; then
		targetDir="${1:?}"; shift 1
	fi

	if [[ -z "${targetDir:-}" ]]; then
		targetDir = "${PWD}"
	fi

	[[ ! -d "${targetDir}" || ! -x "${targetDir}" ]] \
		&& echo "ERROR: unable to access target dir  -- ${targetDir}" >&2 \
		&& return 2

	if [[ ${ISCOLORED} -eq 0 ]]; then
		/usr/local/bin/tree -aFfC "${targetDir}" | grep -v '\/$'
	else
		/usr/local/bin/tree -aFf "${targetDir}" | grep -v '\/$'
	fi

	return $?
}


