# bash functions

if [ -d ~/.bashrc.d/modules/functions ]; then
	
	function listFunctionFiles {
		local target
		target=~/.bashrc.d/modules/functions
		find "$target" -mindepth 1 -maxdepth 1 -type f \! \( -name ".*" -or -name "_*" \) 2> /dev/null
		return $?
	}

	while read -r filePath; do
		[ -r "$filePath" ] && . ${filePath}
	done < <(listFunctionFiles)

	unset -f listFunctionFiles
fi
