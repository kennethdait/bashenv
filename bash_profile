#!/bin/bash

function main {

	# bashdb

	if [[ -z "$BASHENV" ]]; then export BASHENV=~/Developer/bashenv; fi

	#shellcheck source=/Users/kend/Developer/bashenv/.bash_db_credentials
	if [ -r $BASHENV/.bash_db_credentials ]; then
		. $BASHENV/.bash_db_credentials
	fi

	#shellcheck source=/Users/kend/.bashrc
	[ -f ~/.bashrc ] && . ~/.bashrc

	# MY INFO

	export EDITOR=/usr/bin/vim
	export SUDOEDITOR=${EDITOR}

	export MYNAME="Kenneth Dait"
	export MYEMAIL="kpdait@gmail.com"

    export DEFAULTLIGHTROOM='cc'
    export DEFAULTLIGHTROOMFULLSCREEN=0
	# set PS1
	#original PS1='\h:\W \u\$ '
	PS1='[\[\e[36;1m\]\W\[\e[0m\]]\$ '

	# shell options
	shopt -s cdable_vars
	export HISTSIZE=1000000
	export HISTFILESIZE=1000000
    export CDPATH=.:${HOME}
    export GLOBIGNORE=.CFUserTextEncoding:.DS_Store:.localized:.Trash

	# location vars

	verifyLocationAndExport dev ~/Developer
	verifyLocationAndExport desk ~/Desktop
	verifyLocationAndExport docs ~/Documents
	verifyLocationAndExport dl ~/Downloads
	verifyLocationAndExport home ~
	verifyLocationAndExport bashenv ~/Developer/bashenv
	verifyLocationAndExport xcode ~/Developer/xcode
	verifyLocationAndExport	fav ~/Library/Favorites
    verifyLocationAndExport icloud ~/Library/Favorites/icloud/
	verifyLocationAndExport www ~/Sites
	verifyLocationAndExport scripts ~/Developer/scripts
    verifyLocationAndExport xcuserdata ~/Library/Developer/Xcode/DerivedData
    verifyLocationAndExport sites ~/Sites
    verifyLocationAndExport funcs ~/Developer/bashenv/bashrc.d/modules/functions

	# PATH

	verifyAndAppendToPath "/Users/${USER}/.bin" "pre"

	configureGPG &> /dev/null

}

verifyAndAppendToPath() {

	addToPath() {
		#
		# add to path
		#
		case "${cardinality}" in
			pre*)
				#
				# prepend to path
				#
				PATH="${path}${PATH:+:$PATH}"
				;;
			ap*)
				#
				# append to path
				#
				PATH="${PATH:+$PATH:}${path}"
				;;
			*)
				echo "ERROR: there was an error while settings PATH variable. See \`bash_profile'" >&2
				return 1
				;;
		esac
	}

	local path RE cardinality result
	path="${1:?}" cardinality="${2:?}"
	RE="$(echo "$path" | sed -e 's:\.:\\&:g' -e 's:\/:\\&:g')"
	result="$(echo -e ${PATH//:/\\n} | grep "${RE}")"
	if [[ -z "$result" ]]; then
		#
		# not yet in path, add to path
		#
		addToPath
		return 0
	elif [[ -n "$result" && "$result" == "$path" ]]; then
		#
		# already exists in path, return 0
		#
		return 0
	else
		#
		# uncaught error occurred
		#
		echo "ERROR: unable to add to path -- ${path}" >&2
		return 1
	fi
}

verifyLocationAndExport() {
	local varName="${1:?}" path="${2:?}"
	if [[ -d "${path}" && -x "${path}" && -z ${!varName} ]]; then
		eval "export ${varName}=${path}"
		[ $? -eq 0 ] \
			&& eval "locations[\"${varName}\"]=\"$path\""
		return $?
	elif [[ -d "${path}" && -x "${path}" && -n "${!varName}" ]]; then
		if [[ "${!varName}" == "${path}" ]]; then
			return 0
		else
			echo "ERROR" >&2
            echo "Unable to set location variable: ${varName}. Already set to ${!varName}." >&2
			return 1
		fi
	else
		echo "ERROR: an error occurred while setting location variable for ${path}" >&2
		return 1
	fi
}

configureGPG()
{
	#
	# GPG
	#

	#This ensures that SSH can ‘see’ your GPG keys and automatically starts gpg-agent as needed.
	#[source](https://www.linode.com/docs/security/authentication/gpg-key-for-ssh-authentication/)
	#
	#	- gpg config: ~/.gnupg/gpg-agent.conf
	#		- read by gpg-agent on startup
	#		- You should backup this file

	GPG_TTY=$(/usr/bin/tty)
	SSH_AUTH_SOCK="$HOME/.gnupg/S.gpg-agent.ssh"
	export GPG_TTY SSH_AUTH_SOCK

	[ -f ~/.gpg-agent-info ] && source ~/.gpg-agent-info
	if [ -S "${GPG_AGENT_INFO%%:*}" ]; then
	echo found
	    export GPG_AGENT_INFO
	    export SSH_AUTH_SOCK
	    export SSH_AGENT_PID
	else
		if [[ -z "$(ps -f "$(pgrep gpg-agent)" | sed '1d' | sed 's:^[^-]*\(gpg-agent\):\1:')" ]]; then
		    #eval $( gpg-agent --daemon --write-env-file ~/.gpg-agent-info )
		    eval $( gpg-agent --daemon ~/.gpg-agent-info )
		fi
	fi

}



if [ -f /usr/local/share/bash-completion/bash_completion ]; then
  . /usr/local/share/bash-completion/bash_completion
fi

if [[ -n "$PS1" ]] && echo "$-" | grep -q 'i'; then
declare -A locations
main
fi

unset -f main verifyAndAppendToPath addToPath verifyLocationAndExport
