# bashrc

if [ -L ~/.bashrc.d ]; then
	[ -f ~/.bashrc.d/aliases.bash ] && . ~/.bashrc.d/aliases.bash
	[ -f ~/.bashrc.d/functions.bash ] && . ~/.bashrc.d/functions.bash
fi
