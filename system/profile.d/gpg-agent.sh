#!/bin/sh

#envfile="${HOME}/.gnupg/gpg-agent.env"

if test -f "$envfile" \
    && kill -0 $(grep GPG_AGENT_INFO "$envfile" | cut -d: -f 2) 2>/dev/null

then

    eval "$(cat "$envfile")"

else
    return
    #eval "$(gpg-agent --daemon --write-env-file "$envfile")"
fi

[[ -n "$GPG_AGENT_INFO" ]] \
    && export GPG_AGENT_INFO  # the env file does not contain the export statement

# Since v2.1 GnuPG have changed the method a daemon starts. They are all started
# on demand now. For more information see:
#   https://www.gnupg.org/faq/whats-new-in-2.1.html#autostart

#gpgconf --launch gpg-agent

